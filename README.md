## Steps to test the trained Customized VGG16 and Customized Xception model
--------------------------------------------------------------------------
1. Download the repository (The size is approximately 1.3 GB)
2. Now run our trained **TEST OUR TRAINED MODELS.ipynb** notebook


## Folder Structure
---------------------------------------------------------
1. **Data** folder contains training data, validation data, example image, saved trained models
2. **Models** folder contains all our tried out models




## Models that we implemented
---------------------------------------------------------
1. Bag of Features Model (Accuracy ~15%)
2. Simple Neural Network (Accuracy ~23%)
3. CNN (Accuracy ~40%)
4. Transfer Learning using VGG16 pretrained model (Accuracy ~73%)
5. Transfer Learning using Xception pretrained model (Accuracy ~94%)